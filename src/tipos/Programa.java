package tipos;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Formatter;

public class Programa {
	
	public static void main(String[] args) throws FileNotFoundException {
		
		String arquivoDeOrigem = "C:\\_ws\\dev3e5\\ws\\ExemploTipos\\src\\arquivo.txt";
		String arquivoDeDestino = "C:\\_ws\\dev3e5\\ws\\ExemploTipos\\src\\destino.txt";
		File arquivo = new File(arquivoDeOrigem);
		Scanner leitor = new Scanner(arquivo);
		Formatter gravador = new Formatter(arquivoDeDestino);		
		
		Produto[] vetorDeProdutos = new Produto[2];

		int contador = 0;
		while(leitor.hasNextLine()) {
			//1;Computador;Dell;3500.00
			String linha = leitor.nextLine();
			String[] valores = linha.split(";");
			
			Produto p = new Produto();
			p.codigo = Integer.parseInt(valores[0]);
			p.nome = valores[1];
			p.marca = valores[2];
			p.valor = Double.parseDouble(valores[3]);
			
			vetorDeProdutos[contador] = p;
			contador++;
		}
		
		int qtdeProdutos = 0;
		double valorProdutos = 0;
		
		System.out.println("Lista de produtos");		
		gravador.format("Lista de produtos\n\n");
		
		System.out.println();
		for (int i = 0; i < vetorDeProdutos.length; i++) {
			
			Produto p = vetorDeProdutos[i];
			System.out.println("Código " + p.codigo);			
			System.out.println("Nome " + p.nome);
			System.out.println("Marca " + p.marca);
			System.out.println("Valor " + p.valor);
			System.out.println();
			
			gravador.format("Código " + p.codigo + "\n");
			gravador.format("Nome " + p.nome + "\n");
			gravador.format("Marca " + p.marca + "\n");
			gravador.format("Valor " + p.valor + "\n\n");
			
			qtdeProdutos++;
			valorProdutos += p.valor;
		}
		
		System.out.println("Qtde de produtos " + qtdeProdutos);
		System.out.println("Total dos produtos " + valorProdutos);
		
		gravador.format("Qtde de produtos " + qtdeProdutos + "\n");
		gravador.format("Total dos produtos " + valorProdutos + "\n");
		
		leitor.close();
		gravador.close();
	}
	
	
//    public static void main(String[] args) throws FileNotFoundException {
//		
//		Produto meuProduto = new Produto();
//		meuProduto.codigo = 1;
//		meuProduto.nome = "Computador";
//		meuProduto.marca = "Dell";
//		meuProduto.valor = 3500.00;
//		
//		Produto outroProduto = new Produto();
//		outroProduto.codigo = 2;
//		outroProduto.nome = "Smartphone";
//		outroProduto.marca = "iPhone";
//		outroProduto.valor = 5500.00;	
//		
//		Produto[] vetorDeProdutos = new Produto[2];
//		vetorDeProdutos[0] = meuProduto;
//		vetorDeProdutos[1] = outroProduto;
//		
//		System.out.println("Lista de produtos");
//		for (int i = 0; i < vetorDeProdutos.length; i++) {
//			
//			Produto p = vetorDeProdutos[i];
//			System.out.println("Código " + p.codigo);
//			System.out.println("Nome " + p.nome);
//			System.out.println("Marca " + p.marca);
//			System.out.println("Valor " + p.valor);
//			System.out.println();
//		}
//	}

}
